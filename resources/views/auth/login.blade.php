<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Geocoder</title>
        <meta name="description" content="Geocoder">
        <meta name="author" content="KSDI, KSITM">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" integrity="sha512-stI6dFNqqOFkPG83D0fOm3+lQu9LUv8ifVdyRrfUIE68MtFPDz/d322WUR9zzgv190m+4e/ovRctOaZN/a53iw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css" integrity="sha512-EZLkOqwILORob+p0BXZc+Vm3RgJBOe1Iq/0fiI7r/wJgzOFZMlsqTa29UEl6v6U6gsV4uIpsNZoV32YZqrCRCQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
        <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    </head>
    <body>
        <div id="wrapper" class="clear">
            <div id="content-left" class="container">
	            <div class="row md-only">
	                <div class="twelve columns">
                        <a href="{{ url('/login')}}" style="text-decoration: none;">
	                        <img src="{{ asset('images/logo.png') }}" alt="Geocoder" />
                            <h1>Geocoder Portal</h1>
                        </a>
	                </div>
	            </div>
            </div>
            <div id="content-right" class="container">
	            <div id="content-top" class="row sm-only">
	                <div class="twelve columns">
                        <img src="{{ asset('images/logo.png') }}" alt="Geocoder" />
	                    <h1>Geocoder Portal</h1>
	                </div>
	            </div>
	            <div class="row">
	                <div class="twelve columns">
	                    <div id="form-box">
                            <div id="form-title">Access your account</div>
                            <form method="post" action="{{ route('login') }}">
                                <input id="email" name="email" placeholder="Email" required type="email" />
                                
                                <input id="password" name="password" placeholder="Password" required type="password" autocomplete="off" />

                                @csrf
                                
                                <button type="submit" class="app-button btn--fill">Log In</button>
                            </form>
	                    </div>
	                </div>
	            </div>
            </div>
        </div>
    <script type="text/javascript">
    var email = document.querySelector("[name='email']");
    var password = document.querySelector("[name='password']");
    var token = document.querySelector("[name='_token']");

    password.addEventListener("keyup", function(event){
        if(event.keyCode === 13){
            event.preventDefault();
            attemptLogin();
        }
    });

    function attemptLogin() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.responseType = 'json';

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status == 200) {
                var response = xmlhttp.response;
                console.log(response);
                if(response.success && response.api_key != ''){
                    sessionStorage.setItem('api_key',response.api_key);
                    window.location = "{{route('home')}}";
                } else {
                    alert('Access denied');
                }
            }
            else if (xmlhttp.status == 401) {
                alert('Access denied');
            }
            else {
                alert('Some Error occured! Please try again.');
            }
            }
        };
        var params = {'email': email.value, 'password': password.value, '_token': token.value};
        xmlhttp.open("POST", "{{route('login')}}", true);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlhttp.setRequestHeader("X-CSRF-TOKEN",token.value);
        xmlhttp.send(JSON.stringify(params));
    }
    </script>
    </body>
</html>