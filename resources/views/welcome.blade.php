<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>KSDI - Geocoder</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
    <link rel="stylesheet" href="{{ asset('css/atom-one-light.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
</head>
<body>
  <div id="container">
    <div id="side-pane">
      <div class="tab">
        <button id="btn-geocode" class="tablinks" onclick="openTab(event, 'geocode')">Geocode<br/>Lookup</button>
        <button id="btn-reverse" class="tablinks" onclick="openTab(event, 'reverse')">Reverse<br/>Geocode</button>
      </div>
      <div id="side-pane__geocode" class="tabcontent">
        <h3>Geocode</h3>
        <div><pre><code class="language-json" id="geocoder-results">Please use the search box to see result</code></pre></div>
        <div class="loggedOut">
            <div class="footer">
                <a href="{{ url('/login') }}">
                    <button type="button" class="btn">Login</button>
                </a>
                <p>copyright © {{ now()->year }} KSDI, Kerala State IT Mission</p>
            </div>
        </div>
      </div>
      <div id="side-pane__reverse" class="tabcontent">
        <h3>Reverse</h3>
        <div><pre><code class="language-json" id="reverse-results">Please use the search box to see result</code></pre></div>
        <div class="loggedOut">
            <div class="footer">
                <a href="{{ url('/login') }}">
                    <button type="button" class="btn">Login</button>
                </a>
                <p>copyright © {{ now()->year }} KSDI, Kerala State IT Mission</p>
            </div>
        </div>
      </div>
    </div>
    <div id="map"></div>
    </div>
    <div id="site-loader">
        <div id="site-loader__content">
            <img src="{{ asset('images/loading.gif') }}" alt="loading icon" />
        </div>
    </div>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src="{{ asset('js/leaflet.photon.js') }}"></script>
    <script src="{{ asset('js/ajax-jquery.js') }}"></script>
    <script src="{{ asset('js/highlight.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>