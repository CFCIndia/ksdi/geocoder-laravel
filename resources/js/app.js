TILELAYER = 'https://tiles.mapmykerala.in/ksdi/{z}/{x}/{y}.png';
var searchPoints = L.geoJson(null, {
    onEachFeature: function (feature, layer) {
        layer.bindPopup(feature.properties.name);
    }
});
function showSearchPoints(geojson, tab = 'geocoder') {
    document.getElementById(tab + '-results').textContent = JSON.stringify(geojson, undefined, 2)
    hljs.highlightAll()
    searchPoints.clearLayers();
    console.log(geojson)
    searchPoints.addData(geojson);
}
var map = L.map(
    'map', { zoomControl: false, photonControl: true, photonControlOptions: { resultsHandler: showSearchPoints, placeholder: 'Search...', position: 'topleft', url: '/api/?', limit: 10 } }
);
map.setView([10.63422, 76.14457], 8);
map.doubleClickZoom.disable();
searchPoints.addTo(map);
var tilelayer = L.tileLayer(
    TILELAYER, { maxZoom: 18, attribution: '\u00a9 <a href="http://www.openstreetmap.org/copyright"> OpenStreetMap Contributors </a>' }
).addTo(map);
var zoomControl = new L.Control.Zoom({ position: 'bottomright' }).addTo(map);

geocodeForm = document.getElementsByClassName('photon-input')[0]
reverseForm = (
    "<form id='reverse-form' style='display:none;'>\
           <input class='reverse-form-input' type='number' min='0' step='any' placeholder='Latitude' name='rlatitude' id='rlatitude' required />\
           <input class='reverse-form-input' type='number' min='0' step='any' placeholder='longitude' name='rlongitude' id='rlongitude' required />\
           <button id='reverse-form-submit' type='submit'>search</button>\
     </form>"
)
document.getElementsByClassName('leaflet-photon')[0].insertAdjacentHTML('beforeEnd', reverseForm)

// manage side panel
function openTab(e, tabType) {
    tabBlocks = document.getElementsByClassName("tabcontent");
    Array.prototype.forEach.call(tabBlocks, block => {
        block.style.display = "none";
    });
    tabLinks = document.getElementsByClassName("tablinks");
    Array.prototype.forEach.call(tabLinks, block => {
        block.className = block.className.replace(" active", "");
    });

    document.getElementById('side-pane__' + tabType).style.display = "block";
    e.currentTarget.className += " active";

    reverseForm = document.getElementById('reverse-form')
    if (tabType == 'geocode') {
        geocodeForm.style.display = "block"
        reverseForm.style.display = "none"
    } else {
        geocodeForm.style.display = "none"
        reverseForm.style.display = "block"
    }
}

queryParams = new URLSearchParams(location.search);
if (queryParams.get('rlatitude') && queryParams.get('rlongitude')) {
    document.getElementById("btn-reverse").click();
    document.getElementById("rlatitude").value = parseFloat(queryParams.get('rlatitude'))
    document.getElementById("rlongitude").value = parseFloat(queryParams.get('rlongitude'))
    geojson = makeReverseAPICall(
        queryParams.get('rlatitude'), queryParams.get('rlongitude')
    )
} else {
    document.getElementById("btn-geocode").click();
}

function makeReverseAPICall(lat, lon) {
    request = new XMLHttpRequest();
    request.open(
        'GET', `/reverse?lon=${lon}&lat=${lat}`
    )
    request.send()
    request.onload = async function () {
        data = JSON.parse(this.response)
        showSearchPoints(data, 'reverse')
    }
}
