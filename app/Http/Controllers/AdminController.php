<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Reader;
use App\Jobs\MakeApiRequest;
use Illuminate\Support\Str;
use App\Models\GeocodeJob;
use Auth;
use Log;
use Validator;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process file.
     *
     * @return void
     */
    public function batchGeocode(Request $request) {
        $validator = Validator::make($request->all(),[
            'geocode' => 'required|mimes:csv,txt',
        ]);

        if ($validator->fails()) {
            return(['success' => false, 'message' => 'Invalid file! Upload valid csv file', 'data' => $validator->errors()->all()]);
        }

        if($request->hasfile('geocode')){
            // Get uploaded CSV file
            $file = $request->file('geocode');

            // Create list name
            $name = time().'-'.$file->getClientOriginalName();

            // Create a CSV reader instance
            $csv = Reader::createFromPath($file->getRealPath());
            $csv->setHeaderOffset(0);

            $header = $csv->getHeader(); //returns the CSV header record
            if(!in_array('Address',$header) OR !in_array('DISTRICT',$header) OR !in_array('Latitude',$header) OR !in_array('Longitude',$header))
                return(['success' => false, 'message' => 'CSV file does not contain required headers "Address", "DISTRICT", "Latitude", "Longitude" as in sample file! Upload valid csv file']);
            $extra_columns = [
                "Out0_latitude","Out0_longitude",
                "Out1_latitude","Out1_longitude",
                "Out2_latitude","Out2_longitude"
            ];
            $records = $csv->getRecords();
            $data = [];
            foreach ($records as $offset => $record) {
                $data[] = $record;
            }

            $header = array_merge($header,$extra_columns);
            $filename = Str::random(12);
            $handle = fopen(storage_path("app/public/output/".$filename.".csv"), "w");
            fputcsv($handle, $header);
            fclose($handle);

            GeocodeJob::create(['filename' => $filename, 'user_id' => Auth::user()->id]);

            // Create from each row in the CSV file
            $queue = dispatch(new MakeApiRequest($data, $filename));
            return response()->json(['success' => true, 'message' => 'File started processing!', 'data' => $filename]);
        }
        else
            return response()->json(['success' => false, 'message' => 'No file uploaded!']);
    }

    /**
     * Logout user.
     *
     * @return void
     */
    public function logout(Request $request){
        $response = ['success' => false, 'message' => 'User log out failed.'];
        $user = Auth::user();
        $user->api_key = "";
        $logout = $user->save();
        if($logout)
            $response = ['success' => true, 'message' => 'User logged out successfully.'];
        return response()->json($response);
    }

    /**
     * Check job completion.
     *
     * @return void
     */
    public function getJobStatus(Request $request){
        $filename = $request->id;
        $status = GeocodeJob::where('filename', $filename)->value('status');
        return response()->json(['success' => true, 'message' => $status]);
    }
}