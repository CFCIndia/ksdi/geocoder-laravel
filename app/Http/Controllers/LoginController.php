<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /**
     * Returns login page
     */
    public function index(Request $request){
        return view('welcome')->with('csrf_token', $request->session()->get('_token'));
    }

    /**
     * Authenticate user.
     *
     * @return void
     */
    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();
        if(isset($user) AND Hash::check($request->password, $user->password)){
            $apikey = $user->api_key;
            if($apikey == ''){
                $apikey = base64_encode(Str::random(40));
                User::where('email', $request->input('email'))->update(['api_key' => "$apikey"]);
            }
            $request->session()->put('api_key', $apikey);
            return redirect('/');
        } else{
            abort(401);
            return response()->json(['success' => false ], 401);
        }
    }
}
