<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Http;
use Log;
use League\Csv\Writer;
use App\Models\GeocodeJob;

class MakeApiRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $records, $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($records, $filename)
    {
        $this->records = $records;
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $uuid = $this->job->uuid();
        GeocodeJob::where('filename', $this->filename)->update(['job_uuid' => $uuid, 'status' => 'processing']);

        $csv_file = fopen(storage_path("app/public/output/".$this->filename.".csv"), "a");
        foreach($this->records as $index => $record) {
            $params = [
                'q' => $record['Address'].", ".$record['DISTRICT'],
                'limit' => 3
            ];
            if($record['Latitude'] != '' OR $record['Longitude'] != '') {
                $params['lat'] = $record['Latitude'];
                $params['lon'] = $record['Longitude'];
            }
            $response = Http::withoutVerifying()
                ->get(env('APP_URL').'/api', $params);
            if ($response->failed()) {
                Log::error("Failed to process");
                Log::error($output_data);
                GeocodeJob::where('filename', $this->filename)->update(['status' => 'failed']);
                throw new RuntimeException("Failed to connect ", $response->status());
            }
            $result_arr = $response->json();
            $output_data = $record;
            foreach($result_arr as $results){
                if(gettype($results)=='array')
                    foreach($results as $index => $result) {
                        if(isset($result['geometry']['coordinates'])){
                            $lat = $result['geometry']['coordinates'][1];
                            $long = $result['geometry']['coordinates'][0];

                            $lat_key = 'Out'.$index.'_latitude';
                            $long_key = 'Out'.$index.'_longitude';

                            $output_data[$lat_key] = $lat;
                            $output_data[$long_key] = $long;
                        }
                    }
            }
            fputcsv($csv_file, $output_data);
        }
        fclose($csv_file);
    }

    public function failed()
    {
        Log::error("Failed to process!!!");
        GeocodeJob::where('filename', $this->filename)->update(['status' => 'failed']);
    }
}
