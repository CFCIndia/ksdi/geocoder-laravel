<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\URL;
use App\Models\GeocodeJob;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env('APP_ENV') == 'production')
            URL::forceScheme('https');

        Queue::before(function (JobProcessing $event) {
            $uuid = $event->job->uuid();
            Log::info("Queue starting");
            Log::info("UUID: ".$uuid);
        });

        Queue::after(function (JobProcessed $event) {
            $uuid = $event->job->uuid();
            Log::info("Queue end");
            Log::info("UUID: ".$uuid);
            GeocodeJob::where('job_uuid', $uuid)->update(['status' => 'completed']);
        });
    }
}
